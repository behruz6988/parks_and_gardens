jQuery( document ).ready(function( $ ) {
    $( '.slider-pro' ).sliderPro({
        width: 720,
        height: 425,
        orientation: 'vertical',
        loop: false,
        arrows: false,
        buttons: false,
        thumbnailsPosition: 'left',
        thumbnailPointer: false,
        thumbnailWidth: 290,
        breakpoints: {
            800: {
                thumbnailsPosition: 'bottom',
                thumbnailWidth: 270,
                thumbnailHeight: 100
            },
            500: {
                thumbnailsPosition: 'bottom',
                thumbnailWidth: 120,
                thumbnailHeight: 50
            }
        }
    });

    $(".owl-carousel").owlCarousel({

        autoPlay: 6000, //Set AutoPlay to 6 seconds

        items: 1,
        autoplay: true,
        lazyLoad: true,
        loop: true,
        margin: 10,
        responsive: true,
        nav: true,
        navigation: false,
        autoplayHoverPause: true,
        dots: false,
        responsive: true,
        navigationText: ['&#10094;', '&#10095;']

    });

    $('.img-ms-form').click(function(){
        $(this).next('label.radio-l').find('input[type="radio"]').prop('checked', true);
        $(this).addClass('glyphicon glyphicon-ok-circle');
    });

    $('.smenu, .smenu-transparent ').hover(function(){
        $('.smenu-panel, .smenu-transparent').css('display','block');
    },function(){
        $('.smenu-panel,.smenu-transparent').delay(1000).css('display','none');
    });

    $('.img-ms-form').hover(function(){
        //$('.img-ms-form').removeClass('glyphicon glyphicon-ok-circle');
        $(this).addClass('glyphicon glyphicon-ok-circle');
    },function(){
        $(this).removeClass('glyphicon glyphicon-ok-circle');
    });

});
